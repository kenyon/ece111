/*
 * WEP encryption module.
 *
 * http://en.wikipedia.org/wiki/RC4
 *
 * By Kenyon Ralph <kralph@ucsd.edu>
 */

module wep_encrypt (
    clk,
    nreset,
    start_encrypt,
    plain_addr,
    frame_size,
    cipher_addr,
    seed_msw,
    seed_lsw,
    done,
    port_A_clk,
    port_A_data_in,
    port_A_data_out,
    port_A_addr,
    port_A_we
);

    input clk;
    input nreset;

    input start_encrypt;
    // Tells wep_encrypt to start encrypting the given frame.

    input [31:0] plain_addr;
    // Starting address of the plaintext frame. That is, specifies from where
    // wep_encrypt must read the plaintext frame.

    input [31:0] frame_size;
    // Length of the frame in bytes.

    input [31:0] cipher_addr;
    // Starting address of the ciphertext frame. That is, specifies where
    // wep_encrypt must write the ciphertext frame.

    input [31:0] seed_msw;
    // Contains the 4 most significant bytes of the 64 bit seed.

    input [31:0] seed_lsw;
    // Contains the 4 least significant bytes of the 64 bit seed.

    input [31:0] port_A_data_out;
    // Read data from the dpsram (plaintext).

    output [31:0] port_A_data_in;
    // Write data to the dpsram (ciphertext).

    output [15:0] port_A_addr;
    // Address of dpsram being read/written.

    output port_A_clk;
    // Clock to dpsram (drive this with the input clk).

    output port_A_we;
    // Write enable for dpsram.

    output done;
    // Signal to indicate that encryption of the frame is complete.

    reg		done;
    reg [3:0]	state;
    integer	encrypted_bytes;
    integer	i;
    integer	j;
    reg [7:0]	temp;
    reg [7:0]	S [0:255];
    reg [31:0]	plainword;
    reg [7:0]	plainbyte3;
    reg [7:0]	plainbyte2;
    reg [7:0]	plainbyte1;
    reg [7:0]	plainbyte0;
    reg [31:0]	cipherword;
    reg [7:0]	cipherbyte3;
    reg [7:0]	cipherbyte2;
    reg [7:0]	cipherbyte1;
    reg [7:0]	cipherbyte0;
    reg [31:0]	port_A_data_in;
    reg [15:0]	port_A_addr;
    reg		port_A_we;
    reg [15:0]	current_cipher_addr;
    reg [15:0]	current_plain_addr;

    assign port_A_clk	=	clk;

    // States.
    parameter RESET	 = 'd0;
    parameter KSAINIT	 = 'd1; // key-scheduling algorithm
    parameter KSASHUFFLE1= 'd2;
    parameter PREREAD	 = 'd3;
    parameter READWORD	 = 'd4;
    parameter PRGA	 = 'd5; // pseudo-random generation algorithm
    parameter CRYPTBYTE3 = 'd6;
    parameter CRYPTBYTE2 = 'd7;
    parameter CRYPTBYTE1 = 'd8;
    parameter CRYPTBYTE0 = 'd9;
    parameter PREWRITE	 = 'd10;
    parameter WRITEWAIT	 = 'd11;
    parameter WRITEWORD	 = 'd12;
    parameter END	 = 'd13;
    parameter KSASHUFFLE2= 'd14;

    always @(posedge clk or negedge nreset)
    begin
	if (!nreset)
	begin
	    port_A_addr <= 0;
	    port_A_we <= 0;
	    port_A_data_in <= 0;
	    current_plain_addr <= 0;
	    current_cipher_addr <= 0;
	    state <= RESET;
	end

	else
	begin
	    case (state)
		RESET:
		begin
		    //$display("   [RESET] current_plain_addr=%2d\n", current_plain_addr);
		    encrypted_bytes <= 0;
		    done <= 0;
		    j = 0;

		    if (start_encrypt)
		    begin
			current_plain_addr <= plain_addr[15:0];
			current_cipher_addr <= cipher_addr[15:0];
			state <= KSAINIT;
		    end
		end

		KSAINIT:
		begin
		    //$display("   [KSAINIT] current_plain_addr: %2d, current_cipher_addr: %3d\n",
			     //current_plain_addr, current_cipher_addr);

		    for (i = 0; i < 256; i = i + 1)
		    begin
			S[i] = i;
		    end

		    i = 0;
		    state <= KSASHUFFLE1;
		end

		// TODO: pre-get seed bytes instead of having a function do it
		// every time?

		KSASHUFFLE1: // i even
		begin
		    //$display("   [KSASHUFFLE1] i=%3d, j=%3d ", i, j);
		    j = (j + S[i] + seedbyte(i % 8)) % 256;

		    temp = S[i];
		    S[i] = S[j];
		    S[j] = temp;

		    i = i + 1;

		    state <= KSASHUFFLE2;
		    //$display("end of KSASHUFFLE1.\n");
		end

		KSASHUFFLE2: // i odd
		begin
		    //$display("   [KSASHUFFLE2] i=%3d\n", i);
		    j = (j + S[i] + seedbyte(i % 8)) % 256;

		    temp = S[i];
		    S[i] = S[j];
		    S[j] = temp;

		    i = i + 1;

		    //$display("   [KSASHUFFLE2] before if block i=%3d\n", i);
		    if (i == 256)
		    begin
			//$display("   [KSASHUFFLE2] going to PREREAD\n");
			j = 0;
			state <= PREREAD;

			/*
			for (i = 0; i < 256; i = i + 1)
			begin
			    $display("   [KSASHUFFLE2] S[%3d]=%3d\n", i, S[i]);
			end
			$stop;
			*/

		    end
		    else
		    begin
			//$display("   [KSASHUFFLE2] going to KSASHUFFLE1\n");
			state <= KSASHUFFLE1;
		    end
		end

		PREREAD:
		begin
		    //$display("   [PREREAD] reading from current_plain_addr: %3d\n", current_plain_addr);
		    port_A_addr <= current_plain_addr;
		    state <= READWORD;
		end

		READWORD:
		begin
		    plainword <= port_A_data_out;
		    current_plain_addr <= current_plain_addr + 16'd4;
		    state <= PRGA;
		end

		PRGA:
		begin
		    //$display("   [PRGA] plainword: %h\n", plainword);
		    // {cipher,plain}byte3 is the least-significant byte in
		    // the word.
		    plainbyte3 <= plainword[7:0];
		    plainbyte2 <= plainword[15:8];
		    plainbyte1 <= plainword[23:16];
		    plainbyte0 <= plainword[31:24];
		    port_A_addr <= current_cipher_addr;
		    state <= CRYPTBYTE3;
		end

		CRYPTBYTE3:
		begin
		    if (encrypted_bytes < frame_size)
		    begin
			cipherword[7:0] <= cryptbyte(plainbyte3);
			encrypted_bytes <= encrypted_bytes + 'd1;
		    end
		    else
		    begin
			cipherword[7:0] <= plainbyte3;
		    end

		    state <= CRYPTBYTE2;
		end

		CRYPTBYTE2:
		begin
		    if (encrypted_bytes < frame_size)
		    begin
			cipherword[15:8] <= cryptbyte(plainbyte2);
			encrypted_bytes <= encrypted_bytes + 'd1;
		    end
		    else
		    begin
			cipherword[15:8] <= plainbyte2;
		    end

		    state <= CRYPTBYTE1;
		end

		CRYPTBYTE1:
		begin
		    if (encrypted_bytes < frame_size)
		    begin
			cipherword[23:16] <= cryptbyte(plainbyte1);
			encrypted_bytes <= encrypted_bytes + 'd1;
		    end
		    else
		    begin
			cipherword[23:16] <= plainbyte1;
		    end

		    state <= CRYPTBYTE0;
		end

		CRYPTBYTE0:
		begin
		    if (encrypted_bytes < frame_size)
		    begin
			cipherword[31:24] <= cryptbyte(plainbyte0);
			encrypted_bytes <= encrypted_bytes + 'd1;
		    end
		    else
		    begin
			cipherword[31:24] <= plainbyte0;
		    end

		    state <= PREWRITE;
		end

		PREWRITE:
		begin
		    //$display("   [PREWRITE] encrypted_bytes: %2d (after possible encryptions)\n", encrypted_bytes);

		    //$display("   [PREWRITE] writing cipherword=%h to current_cipher_addr=%3d\n\n",
			     //cipherword, current_cipher_addr);

		    port_A_we <= 1;
		    port_A_data_in <= cipherword;
		    state <= WRITEWAIT;
		end

		WRITEWAIT:
		begin
		    state <= WRITEWORD;
		end

		WRITEWORD:
		begin
		    port_A_we <= 0;
		    port_A_addr <= current_plain_addr;
		    current_cipher_addr <= current_cipher_addr + 16'd4;

		    if (encrypted_bytes == frame_size)
		    begin
			state <= END;
		    end
		    else
		    begin
			state <= PREREAD;
		    end
		end

		END:
		begin
		    //$display("   [END]: done.\n");
		    done <= 1;
		    state <= RESET;
		end
	    endcase
	end
    end

    function [7:0] cryptbyte;
	input [7:0] plainbyte;
	integer index;

	begin
	    // encrypted_bytes is 0 when this function is first entered for
	    // a frame.

	    //$display("   [CRYPTBYTE] encrypted_bytes=%2d, plainbyte=%h\n", encrypted_bytes, plainbyte);

	    i = encrypted_bytes;

	    i = (i + 1) % 256;
	    j = (j + S[i]) % 256;

	    temp = S[i];
	    S[i] = S[j];
	    S[j] = temp;

	    index = (S[i] + S[j]) % 256;
	    //$display("   [CRYPTBYTE] plainbyte=%h, index=%2d\n", plainbyte, index);
	    cryptbyte = S[index] ^ plainbyte;
	end
    endfunction

    function [7:0] seedbyte;
	input [2:0] index;

	begin
	    case (index)
		0:
		begin
		    seedbyte = seed_lsw[7:0];
		end

		1:
		begin
		    seedbyte = seed_lsw[15:8];
		end

		2:
		begin
		    seedbyte = seed_lsw[23:16];
		end

		3:
		begin
		    seedbyte = seed_lsw[31:24];
		end

		4:
		begin
		    seedbyte = seed_msw[7:0];
		end

		5:
		begin
		    seedbyte = seed_msw[15:8];
		end

		6:
		begin
		    seedbyte = seed_msw[23:16];
		end

		7:
		begin
		    seedbyte = seed_msw[31:24];
		end
	    endcase
	end
    endfunction

endmodule
