/*
 * Fibonacci calculator.
 *
 * Calculates the nth Fibonacci number, where n is a 5-bit integer given on
 * input_s. The done output is asserted when the result (fibo_out) is valid.
 * The begin_fibo signal must be high for fibo_out to be calculated.
 *
 * When reset_n is low, the first two numbers of the sequence are initialized
 * to 0 and 1.
 *
 * Handles up to Fibonacci number n=24.
 *
 * See http://www.research.att.com/~njas/sequences/table?a=45&fmt=4 for a list
 * of the sequence.
 *
 * By Kenyon Ralph <kralph@ucsd.edu>
 */

module
fibonacci_calculator (
    clk,
    reset_n,
    input_s,
    begin_fibo,
    done,
    fibo_out
);

    input clk;
    input reset_n;
    input [4:0] input_s;
    input begin_fibo;

    output done;
    output [15:0] fibo_out;

    reg done;
    reg [15:0] fibo_out;
    reg [2:0] state;
    reg [2:0] previous_state;
    reg [15:0] s1fib0;
    reg [15:0] s1fib1;
    reg [15:0] s2fib0;
    reg [15:0] s2fib1;
    reg [15:0] fib;
    reg [15:0] counter;

    // States.
    parameter RESET = 0;
    parameter S1 = 1;
    parameter S2 = 2;
    parameter END = 3;
    parameter IDLE = 4;

    always @(posedge clk or negedge reset_n)
    begin
	if (!reset_n)
	begin
	    state <= RESET;
	end

	else
	begin
	    case (state)
		RESET:
		begin
		    $display("[RESET] input_s=%2d\n", input_s);
		    previous_state <= RESET;

		    if (counter == input_s)
		    begin
			state <= END;
		    end

		    else if (begin_fibo)
		    begin
			state <= S1;
		    end

		    fib <= 1;
		    fibo_out <= 0;
		    done <= 0;
		    counter <= 1;
		    s1fib0 <= 0;
		    s1fib1 <= 0;
		    s2fib0 <= 0;
		    s2fib1 <= 1;
		end

		S1:
		begin
		    $display("[S1] counter=%3d\n", counter);
		    done <= 0;
		    previous_state <= S1;

		    if (!begin_fibo && counter != input_s)
		    begin
			state <= IDLE;
		    end

		    else if (counter == input_s)
		    begin
			state <= END;
		    end

		    else
		    begin
			state <= S2;

			fib <= s2fib0 + s2fib1;
			s1fib0 <= fib;
			s1fib1 <= s2fib1 + s1fib1;
			counter <= counter + 1;

			fibo_out <= fib;
		    end
		end

		S2:
		begin
		    $display("[S2] counter=%3d\n", counter);
		    done <= 0;
		    previous_state <= S2;

		    if (!begin_fibo && counter != input_s)
		    begin
			state <= IDLE;
		    end

		    else if (counter == input_s)
		    begin
			state <= END;
		    end

		    else
		    begin
			state <= S1;

			fib <= s1fib0 + s1fib1;
			s2fib0 <= fib;
			s2fib1 <= s1fib1 + s2fib1;
			counter <= counter + 1;

			fibo_out <= fib;
		    end
		end

		IDLE:
		begin
		    $display("[IDLE]\n");
		    if (!begin_fibo)
		    begin
			state <= IDLE;
		    end
		    else
		    begin
			previous_state <= IDLE;
			state <= previous_state;
		    end
		end

		END:
		begin
		    previous_state <= END;

		    $display("[END] counter=%3d, fib=%2d\n", counter, fib);
		    state <= RESET;

		    counter <= 1;
		    fibo_out <= fib;
		    done <= 1;
		end

	    endcase
	end

    end
endmodule
