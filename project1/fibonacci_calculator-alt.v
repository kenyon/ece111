/*
 * Fibonacci calculator.
 *
 * Calculates the nth Fibonacci number, where n is a 5-bit integer given on
 * input_s. The done output is asserted when the result (fibo_out) is valid.
 * The begin_fibo signal must be high for fibo_out to be calculated.
 *
 * When reset_n is low, the first two numbers of the sequence are initialized
 * to 0 and 1.
 *
 * Handles up to Fibonacci number n=24.
 *
 * See http://www.research.att.com/~njas/sequences/table?a=45&fmt=4 for a list
 * of the sequence.
 *
 * By Kenyon Ralph <kralph@ucsd.edu>
 */

module
fibonacci_calculator_alt (
    clk,
    reset_n,
    input_s,
    begin_fibo,
    done,
    fibo_out
);

    input clk;
    input reset_n;
    input [4:0] input_s;
    input begin_fibo;

    output done;
    output [15:0] fibo_out;

    reg done;
    reg [15:0] fibo_out;
    reg [4:0] i;
    reg [15:0] a = 1;
    reg [15:0] b = 0;
    reg [15:0] c = 0;
    reg [15:0] d = 1;
    reg [15:0] t;

    // The first two elements in the Fibonacci sequence.
    parameter N0 = 0;
    parameter N1 = 1;

    always @(posedge clk or negedge reset_n)
    begin
	if (!reset_n)
	begin
	    i <= input_s - 1;
	    fibo_out <= N1;
	    done <= 1;
	end

	else if (begin_fibo)
	begin
	    if (input_s <= 0)
	    begin
		fibo_out <= N0;
		done <= 1;
	    end

	    while (i > 0)
	    begin
		if (i % 2 == 1)
		begin
		    t <= d * (b + a) + c * b;
		    a <= d * b + c * a;
		    b <= t;
		end

		t <= d * (2 * c + d);
		c <= c * c + d * d;
		d <= t;
		i <= i / 2;
	    end

	    fibo_out <= a + b;
	    done <= 1;
	end

	else
	begin
	    // what to do here?
	    //fibo_out <= FIXME!
	    done <= 1;
	end
    end
endmodule
