module
tb_fibonacci_calculator;
    reg reset_n;
    reg clk;
    reg [4:0] input_s;
    reg begin_fibo;

    wire done;
    wire [15:0] fibo_out;

    fibonacci_calculator dut (
	.clk(clk),
	.reset_n(reset_n),
	.input_s(input_s),
	.begin_fibo(begin_fibo),
	.done(done),
	.fibo_out(fibo_out)
    );

    initial
    begin
	clk = 0;
	forever #100 clk = ~clk;
    end

    initial
    begin
	@(negedge clk) reset_n <= 0;
	@(negedge clk) begin_fibo <= 0;
	@(negedge clk) input_s <= 20;
	# 400;
	@(negedge clk) reset_n <= 1;
	@(negedge clk) begin_fibo <= 1;

	wait (done == 1);

	@(negedge clk) begin_fibo <= 0;
	@(negedge clk) input_s <= 10;
	# 400;
	@(negedge clk) begin_fibo <= 1;
	# 400;
	@(negedge clk) begin_fibo <= 0;
	# 800;
	@(negedge clk) begin_fibo <= 1;

	wait (done == 1);
	# 4000;

	$stop;
    end
endmodule
